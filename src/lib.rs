use std::path::{Path, PathBuf};

/// Generate a new file derived from an input file name
///
/// # Parameters
///
/// | Parameter     | Description                                                                      |
/// |---------------|----------------------------------------------------------------------------------|
/// | file_name     | The source file name to derive from.                                             |
/// | output_folder | An option folder name that will replace the folder name in the source file name. |
/// | new_extension | The extension to use in the final file name.                                     |
///
/// If `output_folder` is `None`, the current directory (from
/// `std::env::current_dir()`) will be used.
///
/// # Example
///
/// ```rust
/// use std::path::PathBuf;
///
/// let source_file_name = PathBuf::from("/folder/foo.txt");
/// let new_file_name = baad_file_name::derive_file_name(
///     &source_file_name,
///     Some(PathBuf::from("/my_folder")),
///     "bar");
/// assert_eq!(new_file_name, Some(PathBuf::from("/my_folder/foo.bar")));
/// ```
///
pub fn derive_file_name(
    file_name: &Path,
    output_folder: Option<PathBuf>,
    new_extension: &str,
) -> Option<PathBuf> {
    let stem_file_name = PathBuf::from(file_name.file_stem()?);
    let output_folder = match output_folder {
        Some(output_folder) => output_folder,
        None => std::env::current_dir().ok()?,
    };

    Some(output_folder.join(stem_file_name.with_extension(new_extension)))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic() {
        let source = PathBuf::from("/folder/foo.txt");
        let output_folder = PathBuf::from("my_folder");

        let new_file_name = derive_file_name(&source, Some(output_folder), "bar");
        assert_eq!(
            new_file_name,
            Some(PathBuf::from("my_folder").join("foo.bar"))
        );
    }

    #[test]
    fn no_output_folder() {
        let source = PathBuf::from("/folder/foo.txt");

        let current_dir = std::env::current_dir().unwrap();

        let new_file_name = derive_file_name(&source, None, "bar");

        assert_eq!(new_file_name, Some(current_dir.join("foo.bar")));
    }

    #[test]
    fn no_extension() {
        let source = PathBuf::from("/folder/foo.txt");
        let output_folder = PathBuf::from("my_folder");

        let new_file_name = derive_file_name(&source, Some(output_folder), "");
        assert_eq!(new_file_name, Some(PathBuf::from("my_folder").join("foo")));
    }
}
