# Baad-File-Name

This is a simple crate that exposes a single function:

```rust
fn derive_file_name(file_name: &Path, 
                    output_folder: Some(&Path), 
                    new_extension: &Path) -> Option<PathBuf>;
```

It will take the file name `file_name`, and replace its folder part and
extension part.  If an output folder is not given (i.e. `output_folder` is
`None`), the current directory is used instead.

# Examples

| `file_name`         | `output_folder`       | `new_extension` | Result                    |
| ------------------- | --------------------- | --------------- | ------------------------- |
| `"/folder/foo.txt"` | `Some("/new_folder")` | `"bar"`         | `"/new_folder/foo.bar"`   |
| `"/folder/foo.txt"` | `None`                | `"bar"`         | `"<current_dir>/foo.bar"` |

# Motivation

This is useful for writing tools that take an input file and outputs to one or
many files whose file names are derived from the input file.  For example,
imagine a tool that takes a mardown file (with ".md" extension) and produces a
HTML and CSS file with the same file name stem but with extensions ".html" and
".css".
